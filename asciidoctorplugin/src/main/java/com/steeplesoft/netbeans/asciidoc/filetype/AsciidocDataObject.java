/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.steeplesoft.netbeans.asciidoc.filetype;

import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Collection;
import javax.swing.Action;
import org.netbeans.core.spi.multiview.MultiViewElement;
import org.netbeans.core.spi.multiview.text.MultiViewEditorElement;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.MIMEResolver;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectExistsException;
import org.openide.loaders.MultiDataObject;
import org.openide.loaders.MultiFileLoader;
import org.openide.util.Lookup;
import org.openide.util.NbBundle.Messages;
import org.openide.windows.TopComponent;

@Messages({
    "LBL_Asciidoc_LOADER=Files of Asciidoc"
})
@MIMEResolver.ExtensionRegistration(
        displayName = "#LBL_Asciidoc_LOADER",
        mimeType = AsciidocDataObject.MIMETYPE_ASCIIDOC,
        extension = {"ad", "asciidoc", "adoc"}
)
@DataObject.Registration(
        mimeType = AsciidocDataObject.MIMETYPE_ASCIIDOC,
        iconBase = "com/steeplesoft/netbeans/asciidoc/filetype/asciidoctor.png",
        displayName = "#LBL_Asciidoc_LOADER",
        position = 300
)
@ActionReferences({
    @ActionReference(
            path = "Loaders/text/asciidoc/Actions",
            id = @ActionID(category = "System", id = "org.openide.actions.OpenAction"),
            position = 100,
            separatorAfter = 200
    ),
    @ActionReference(
            path = "Loaders/text/asciidoc/Actions",
            id = @ActionID(category = "Edit", id = "org.openide.actions.CutAction"),
            position = 300
    ),
    @ActionReference(
            path = "Loaders/text/asciidoc/Actions",
            id = @ActionID(category = "Edit", id = "org.openide.actions.CopyAction"),
            position = 400,
            separatorAfter = 500
    ),
    @ActionReference(
            path = "Loaders/text/asciidoc/Actions",
            id = @ActionID(category = "Edit", id = "org.openide.actions.DeleteAction"),
            position = 600
    ),
    @ActionReference(
            path = "Loaders/text/asciidoc/Actions",
            id = @ActionID(category = "System", id = "org.openide.actions.RenameAction"),
            position = 700,
            separatorAfter = 800
    ),
//    @ActionReference(
//            path = "Loaders/text/asciidoc/Actions",
//            id = @ActionID(category = "System", id = "org.openide.actions.SaveAsTemplateAction"),
//            position = 900,
//            separatorAfter = 1000
//    ),
//    @ActionReference(
//            path = "Loaders/text/asciidoc/Actions",     
//            id = @ActionID(category = "System", id = "org.openide.actions.FileSystemAction"),
//            position = 1100,
//            separatorAfter = 1200
//    ),
//    @ActionReference(
//            path = "Loaders/text/asciidoc/Actions",
//            id = @ActionID(category = "System", id = "org.openide.actions.ToolsAction"),
//            position = 1300
//    ),
    @ActionReference(
            path = "Loaders/text/asciidoc/Actions",
            id = @ActionID(category = "System", id = "org.openide.actions.PropertiesAction"),
            position = 1400
    )
//    @ActionReference(
//            path = "Loaders/text/asciidoc/Actions",
//            id = @ActionID(category = "System", id = "com.steeplesoft.netbeans.filetype.asciidoc.SwitchLineWrapModeAction"),
//            position = 1500
//    )
})
public class AsciidocDataObject extends MultiDataObject {

    public static final String MIMETYPE_ASCIIDOC = "text/asciidoc";

    public AsciidocDataObject(FileObject pf, MultiFileLoader loader) throws DataObjectExistsException, IOException {
        super(pf, loader);
        registerEditor(MIMETYPE_ASCIIDOC, true);
    }

    @Override
    public Lookup getLookup() {
        final Lookup lookup = super.getLookup(); //To change body of generated methods, choose Tools | Templates.
        Collection<? extends Action> actions = lookup.lookupAll(Action.class);
        Collection<? extends ActionListener> listeners = lookup.lookupAll(ActionListener.class);

        return lookup; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected int associateLookup() {
        return 1;
    }

    @MultiViewElement.Registration(
            displayName = "#LBL_Asciidoc_EDITOR",
            iconBase = "com/steeplesoft/netbeans/asciidoc/filetype/asciidoctor.png",
            mimeType = MIMETYPE_ASCIIDOC,
            persistenceType = TopComponent.PERSISTENCE_ONLY_OPENED,
            preferredID = "Asciidoc",
            position = 1000
    )
    @Messages("LBL_Asciidoc_EDITOR=Source")
    public static MultiViewEditorElement createEditor(Lookup lkp) {
        return new MultiViewEditorElement(lkp);
    }
}
