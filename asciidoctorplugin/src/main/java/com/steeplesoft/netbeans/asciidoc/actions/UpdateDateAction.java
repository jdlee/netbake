/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.steeplesoft.netbeans.asciidoc.actions;

import com.steeplesoft.netbeans.asciidoc.filetype.AsciidocDataObject;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JEditorPane;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionRegistration;
import org.openide.cookies.EditorCookie;
import org.openide.filesystems.FileLock;
import org.openide.filesystems.FileObject;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle.Messages;

@ActionID(
        category = "File",
        id = "com.steeplesoft.netbeans.asciidoc.actions.UpdateDateAction"
)
@ActionRegistration(
        displayName = "#CTL_UpdateDateAction"
)
@ActionReference(path = "Loaders/text/asciidoc/Actions", position = 1500)
@Messages("CTL_UpdateDateAction=Update Post Date")
public final class UpdateDateAction implements ActionListener {
    private final DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    public final Pattern pattern = Pattern.compile("\\d{4}?-\\d{2}?-\\d{2}");
    private final List<AsciidocDataObject> context;

    public UpdateDateAction(List<AsciidocDataObject> context) {
        this.context = context;
    }

    @Override
    public void actionPerformed(ActionEvent ev) {
        for (AsciidocDataObject ado : context) {
            String name = ado.getName();
            Matcher m = pattern.matcher(name);
            final String newDate = sdf.format(new Date());
            String newName = m.replaceFirst(newDate);
            try {
                ado.rename(newName);
//                ado.setModified(true);
                fixupDateInFrontMatter(ado, newDate);
            } catch (IOException ex) {
                Exceptions.printStackTrace(ex);
            }
        }
    }
    
    private void fixupDateInFrontMatter(AsciidocDataObject ado, String newDate) throws IOException {
        try {
            final FileObject pf = ado.getPrimaryFile();
            final EditorCookie ec = ado.getLookup().lookup(EditorCookie.class);
            final JEditorPane[] openedPanes = ec.getOpenedPanes();
            final boolean open = openedPanes.length > 0;
            FileLock lock;
            
            String document;
            if (open) {
                document = openedPanes[0].getText();
            } else {
                lock = pf.lock();
                document = pf.asText();
            }
            
            int startFM = document.indexOf("---\n");
            int endFM = document.indexOf("---\n", 4);
            
            if (startFM > -1 && endFM > -1) {
                String frontMatter = document.substring(startFM  + 4, endFM);
                String content = document.substring(endFM + 4);
                
                
            }

            /*
            boolean complete = false;
            boolean inFrontMatter = false;
            final List<String> lines = pf.asLines();
            final FileLock 
            OutputStream os = pf.getOutputStream(lock);
            
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os));
            
            String newLine;
            for (String line : lines) {
                newLine = line;
                if (line.startsWith("---")) {
                    if (inFrontMatter) {
                        complete = true;
                        inFrontMatter = false;
                    } else if (!complete) {
                        inFrontMatter = true;
                    }
                } else if (line.startsWith("date:") && inFrontMatter) {
                    newLine = "date: " + newDate;
                }
                writer.write(newLine + "\n");
            }
            
            writer.close();
            lock.releaseLock();
            */
        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
        }
    }
}
