/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.steeplesoft.netbeans.asciidoc.filetype;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
import javax.swing.SwingUtilities;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.EditorRegistry;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.settings.SimpleValueNames;
import org.netbeans.modules.editor.NbEditorUtilities;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;

//@ActionID(
//        category = "System",
//        id = "com.steeplesoft.netbeans.asciidoc.filetype.ToggleLineWrapModeAction"
//)
//@ActionRegistration(
//        displayName = "#CTL_ToggleLineWrapModeAction"
//)
//@ActionReferences({
//    @ActionReference(path = "Loaders/text/asciidoc/Actions", position = 1600),
//    @ActionReference(path = "Editors/text/x-java/Popup", position = 1620)
//})

//@Messages("CTL_ToggleLineWrapModeAction=Toggle Line Wrap")
public final class ToggleLineWrapModeAction implements ActionListener {

    private static final String key = SimpleValueNames.TEXT_LINE_WRAP;
    private final AsciidocDataObject context;

    public ToggleLineWrapModeAction(AsciidocDataObject context) {
        this.context = context;
    }

    @Override
    public void actionPerformed(ActionEvent ev) {
        SwingUtilities.invokeLater(new Runnable() {
            public @Override
            void run() {
                JTextComponent jtc = EditorRegistry.lastFocusedComponent();
                if (null == jtc) {
                    return;
                }

                final String mimeType = NbEditorUtilities.getMimeType(jtc);
                final Lookup mime = MimeLookup.getLookup(mimeType);
                final Preferences prefs = mime.lookup(Preferences.class);

                String current = (String) jtc.getDocument().getProperty(key);
                String newMode = "none".equals(current) ? "words" : "none";
                jtc.getDocument().putProperty(key, newMode);
                prefs.put(key, newMode);
                try {
                    prefs.flush();
                } catch (BackingStoreException ex) {
                    Exceptions.printStackTrace(ex);
                }

                jtc.invalidate();
                jtc.updateUI();
                jtc.repaint();
            }
        });
    }
}
