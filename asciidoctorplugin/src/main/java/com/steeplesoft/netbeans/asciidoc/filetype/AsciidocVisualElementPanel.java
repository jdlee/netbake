/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.steeplesoft.netbeans.asciidoc.filetype;

import java.awt.BorderLayout;
import java.io.IOException;
import java.util.Map;
import javax.swing.JPanel;
import org.openide.filesystems.FileChangeAdapter;
import org.openide.filesystems.FileEvent;
import javax.swing.JEditorPane;
import javax.swing.JScrollPane;
//import org.asciidoctor.Asciidoctor;
//import org.asciidoctor.AttributesBuilder;
//import org.asciidoctor.OptionsBuilder;
//import org.asciidoctor.SafeMode;
import org.openide.util.Exceptions;

/**
 *
 * @author jdlee
 */
public class AsciidocVisualElementPanel extends JPanel {

//    private static Asciidoctor instance;
    private static Map<String, Object> options;

    public AsciidocVisualElementPanel(final AsciidocDataObject dobj) {
        setLayout(new BorderLayout());
        final JPanel parent = this;
        dobj.getPrimaryFile().addFileChangeListener(new FileChangeAdapter() {
            @Override
            public void fileChanged(FileEvent fe) {
                parent.removeAll();//removeChildren();
                parent.add(createEditorPane(dobj), BorderLayout.CENTER);
            }
        });
        add(createEditorPane(dobj), BorderLayout.CENTER);
    }

    private JScrollPane createEditorPane(AsciidocDataObject dobj) {
        String source = "";
        try {
            source = dobj.getPrimaryFile().asText().trim();
        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
        }
        if (source.startsWith("---")) {
            int index = source.indexOf("---", 3);
            if (index + 3 < source.length()) {
                source = source.substring(index + 3).trim();
            }
        }
        String rendered ="error!";
        try {
            rendered = AsciidoctorHolderJs.getInstance().render(source);
//            rendered = getAsciidoctorInstance().render(source, Collections.EMPTY_MAP); //, getAsciidoctorOptions());
        } catch (Exception e) {
            rendered = "An error occured: " + e.getMessage();
        }
        rendered = rendered.replace("meta http-equiv=\"Content-Type\"", "meta http-equiv=\"Content-Type1\"");
        JEditorPane preview = new JEditorPane("text/html", rendered);
        preview.setContentType("text/html");
        preview.setText(rendered);
        preview.setEditable(false);
        JScrollPane editorScrollPane = new JScrollPane(preview);
//        editorScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        return editorScrollPane;
    }

    /*
    private static synchronized Asciidoctor getAsciidoctorInstance() {
        if (instance == null) {
            instance = Asciidoctor.Factory.create();
        }

        return instance;
    }

    private static Map<String, Object> getAsciidoctorOptions() {
        if (options == null) {
            // TODO: make configurable. Per project?
            options = OptionsBuilder.options()
                    .compact(false)
                    .headerFooter(false)
                    .safe(SafeMode.UNSAFE)
                    .backend("html5")
                    .attributes(AttributesBuilder.attributes().imagesDir(".").asMap()).asMap();
        }

        return options;
    }
    */
}
