/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.steeplesoft.netbeans.asciidoc.wizard;

import java.awt.Component;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.NoSuchElementException;
import java.util.Set;
import javax.swing.JComponent;
import javax.swing.event.ChangeListener;
import org.netbeans.api.project.Project;
import org.netbeans.api.templates.TemplateRegistration;
import org.netbeans.spi.project.ui.templates.support.Templates;
import org.openide.WizardDescriptor;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.NbBundle.Messages;

@TemplateRegistration(folder = "Asciidoc",
        displayName = "#NewAsciidocFileWizardIterator_displayName",
        iconBase = "com/steeplesoft/netbeans/asciidoc/icon.png")
//        description = "filetype/wizard/newAsciidocFile.html")
@Messages("NewAsciidocFileWizardIterator_displayName=Asciidoc File")
public final class NewAsciidocFileWizardIterator implements WizardDescriptor.InstantiatingIterator<WizardDescriptor> {

    private int index;

    private WizardDescriptor wizard;
    private List<WizardDescriptor.Panel<WizardDescriptor>> panels;

    private List<WizardDescriptor.Panel<WizardDescriptor>> getPanels() {
        if (panels == null) {
            Project project = Templates.getProject(wizard);
            panels = new ArrayList<WizardDescriptor.Panel<WizardDescriptor>>();
            panels.add(new NewAsciidocFileWizardPanel1(project));
            String[] steps = createSteps();
            for (int i = 0; i < panels.size(); i++) {
                Component c = panels.get(i).getComponent();
                if (steps[i] == null) {
                    // Default step name to component name of panel. Mainly
                    // useful for getting the name of the target chooser to
                    // appear in the list of steps.
                    steps[i] = c.getName();
                }
                if (c instanceof JComponent) { // assume Swing components
                    JComponent jc = (JComponent) c;
                    jc.putClientProperty(WizardDescriptor.PROP_CONTENT_SELECTED_INDEX, i);
                    jc.putClientProperty(WizardDescriptor.PROP_CONTENT_DATA, steps);
                    jc.putClientProperty(WizardDescriptor.PROP_AUTO_WIZARD_STYLE, true);
                    jc.putClientProperty(WizardDescriptor.PROP_CONTENT_DISPLAYED, true);
                    jc.putClientProperty(WizardDescriptor.PROP_CONTENT_NUMBERED, true);
                }
            }
        }
        return panels;
    }

    @Override
    public Set<?> instantiate() throws IOException {
        Set<FileObject> resultSet = new LinkedHashSet<FileObject>();
        String location = (String) wizard.getProperty("location");
        final String date = (String) wizard.getProperty("date");
        final Boolean dateInName = (Boolean) wizard.getProperty("dateInName");

        if (Boolean.TRUE.equals(dateInName)) {
            location = date.replace("/", "-") + "-" + location;
        }
        File fileLocation = new File(location);
        File dirF = FileUtil.normalizeFile(fileLocation);
        dirF.mkdirs();
        final File newFile;

        BufferedWriter out = null;
        try {
            String title = (String) wizard.getProperty("title");
            String fileName = sanitizeTitle(title) + ".ad";
            newFile = new File(fileLocation, fileName);
            out = new BufferedWriter(new FileWriter(newFile));

            out.write("---");
            out.newLine();
            outputYamlEntry(out, "title  ", "\"" + title + "\"");
            outputYamlEntry(out, "author ", (String) wizard.getProperty("author"));
            outputYamlEntry(out, "date   ", date);
            outputYamlEntry(out, "layout ", (String) wizard.getProperty("layout"));
            outputYamlEntry(out, "tags   ", "[ " + (String) wizard.getProperty("tags") + " ]");
            out.write("---");
            out.newLine();
            out.newLine();
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (IOException ioe) {

                }
            }
        }

        resultSet.add(FileUtil.toFileObject(newFile));
        Templates.getProject(wizard).getProjectDirectory().refresh(true);

        return resultSet;
    }

    @Override
    public void initialize(WizardDescriptor wizard) {
        this.wizard = wizard;
    }

    @Override
    public void uninitialize(WizardDescriptor wizard) {
        panels = null;
    }

    @Override
    public WizardDescriptor.Panel<WizardDescriptor> current() {
        return getPanels().get(index);
    }

    @Override
    public String name() {
        return index + 1 + ". from " + getPanels().size();
    }

    @Override
    public boolean hasNext() {
        return index < getPanels().size() - 1;
    }

    @Override
    public boolean hasPrevious() {
        return index > 0;
    }

    @Override
    public void nextPanel() {
        if (!hasNext()) {
            throw new NoSuchElementException();
        }
        index++;
    }

    @Override
    public void previousPanel() {
        if (!hasPrevious()) {
            throw new NoSuchElementException();
        }
        index--;
    }

    // If nothing unusual changes in the middle of the wizard, simply:
    @Override
    public void addChangeListener(ChangeListener l) {
    }

    @Override
    public void removeChangeListener(ChangeListener l) {
    }
    // If something changes dynamically (besides moving between panels), e.g.
    // the number of panels changes in response to user input, then use
    // ChangeSupport to implement add/removeChangeListener and call fireChange
    // when needed

    // You could safely ignore this method. Is is here to keep steps which were
    // there before this wizard was instantiated. It should be better handled
    // by NetBeans Wizard API itself rather than needed to be implemented by a
    // client code.
    private String[] createSteps() {
        String[] beforeSteps = (String[]) wizard.getProperty("WizardPanel_contentData");
        assert beforeSteps != null : "This wizard may only be used embedded in the template wizard";
        String[] res = new String[(beforeSteps.length - 1) + panels.size()];
        for (int i = 0; i < res.length; i++) {
            if (i < (beforeSteps.length - 1)) {
                res[i] = beforeSteps[i];
            } else {
                res[i] = panels.get(i - beforeSteps.length + 1).getComponent().getName();
            }
        }
        return res;
    }

    private void outputYamlEntry(BufferedWriter writer, String key, String value) throws IOException {
        writer.write(String.format("%s: %s", key, value));
        writer.newLine();
    }

    public String sanitizeTitle(String title) {
        title = title.toLowerCase(Locale.getDefault())
                .replace(" ", "-")
                .replace(",", "")
                .replaceAll("[!?\\*\\[\\]\\@\\#\\$%^&()\"':\\.,]", ""); // TODO: there has to be a saner way :P
        return title;
    }
}
