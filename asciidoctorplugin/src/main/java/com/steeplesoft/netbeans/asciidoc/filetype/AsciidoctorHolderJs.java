/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.steeplesoft.netbeans.asciidoc.filetype;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import javax.script.Bindings;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.script.SimpleBindings;
import org.openide.util.Exceptions;

/**
 *
 * @author jdlee
 */
public class AsciidoctorHolderJs {
    private final ExecutorService pool = Executors.newFixedThreadPool(5);
//    private ScriptEngine engine;
    private final Future<ScriptEngine> future;

    private AsciidoctorHolderJs() {
        future = pool.submit(new Callable<ScriptEngine>() {

            @Override
            public ScriptEngine call() throws Exception {
                ScriptEngine engine = new ScriptEngineManager().getEngineByName("js");
                try {
                    engine.eval(readFile(getClass().getResourceAsStream("/asciidoctor-all.js")));
                } catch (ScriptException | IOException ex) {
                    Exceptions.printStackTrace(ex);
                }
                return engine;
            }
        });
//        engine = new ScriptEngineManager().getEngineByName("js");
//
//        try {
//            engine.eval(readFile(getClass().getResourceAsStream("/asciidoctor-all.js")));
//        } catch (ScriptException | IOException ex) {
//            Exceptions.printStackTrace(ex);
//        }
    }

    private static class LazyHolder {

        private static final AsciidoctorHolderJs INSTANCE = new AsciidoctorHolderJs();
    }

    public static AsciidoctorHolderJs getInstance() {
        return LazyHolder.INSTANCE;
    }

    public String render(String source) {
        try {
            ScriptEngine engine = future.get();
            
            Bindings bindings = new SimpleBindings();
            engine.put("asciidoc", source);
            System.out.println(source);
            return engine.eval("Opal.Asciidoctor.$convert(asciidoc, Opal.hash2(['doctype', 'attributes'], {doctype: 'inline', attributes: ['showtitle']}));"
                    /*,bindings*/).toString();

        } catch (Exception ex) {
            return ex.getMessage();
        }
    }

    private String readFile(InputStream is) throws IOException {

        StringBuilder textBuilder = new StringBuilder();
        try (Reader reader = new BufferedReader(new InputStreamReader(is))) {
            int c = 0;
            while ((c = reader.read()) != -1) {
                textBuilder.append((char) c);
            }
        }
        return textBuilder.toString();
    }
}
