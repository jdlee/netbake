#!/bin/bash

VERSION=$1

function pkg() {
    TYPE=$1
    fakeroot fpm -s dir \
        -t $TYPE \
        -C fpm \
        -a all \
        --url http://glassfish.org  \
        --name netbake \
        -m "jason@steeplesoft.com" \
        -f \
        --vendor "Steeplesoft"  \
        --version $VERSION \
        --epoch `date +%Y` .
    if [ $? != 0 ] ; then
        exit $?
    fi
}

cd target
mkdir -p fpm/opt/netbake fpm/etc/profile.d
cp -r netbake/* fpm/opt/netbake/
cp ../netbake.sh fpm/etc/profile.d/
pkg deb
pkg rpm
