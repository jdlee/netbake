
import com.steeplesoft.awestruct.netbeans.nodes.ConfigNodeComparator;
import java.awt.Component;
import java.awt.Image;
import java.awt.datatransfer.Transferable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.datatransfer.NewType;
import org.openide.util.datatransfer.PasteType;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author jdlee
 */
public class NodeStortTest {

    @Test
    public void nodeSortTest() {
        List<String[]> nodes = new ArrayList<String[]>() {{
            add(new String[] {"b.txt", "true"});
            add(new String[] {"_config", "false"});
            add(new String[] {"_ext", "false"});
            add(new String[] {"file.ad", "true"});
        }};
        List<Node> result = new ArrayList<Node>();
        for (String[] node : nodes) {
            result.add(new TestNode(node[0], Boolean.parseBoolean(node[1])));
        }

        Collections.sort(result, new ConfigNodeComparator());
        Assert.assertEquals(nodes.get(1)[0], result.get(0).getName());
        Assert.assertEquals(nodes.get(2)[0], result.get(1).getName());
        Assert.assertEquals(nodes.get(0)[0], result.get(2).getName());
        Assert.assertEquals(nodes.get(3)[0], result.get(3).getName());
    }

    private static class TestNode extends Node {

        public TestNode(String name, boolean isLeaf) {
            super(isLeaf ? Children.LEAF : new TestChildren());
            this.setName(name);
        }

        @Override
        public String toString() {
            return "{name: '" + getName() +"', isLeaf: " + isLeaf() + "}";
        }
        
        

        @Override
        public Node cloneNode() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public Image getIcon(int i) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public Image getOpenedIcon(int i) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public HelpCtx getHelpCtx() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public boolean canRename() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public boolean canDestroy() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public PropertySet[] getPropertySets() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public Transferable clipboardCopy() throws IOException {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public Transferable clipboardCut() throws IOException {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public Transferable drag() throws IOException {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public boolean canCopy() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public boolean canCut() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public PasteType[] getPasteTypes(Transferable t) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public PasteType getDropType(Transferable t, int i, int i1) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public NewType[] getNewTypes() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public boolean hasCustomizer() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public Component getCustomizer() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public Handle getHandle() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

    }

    private static class TestChildren extends Children {

        @Override
        public boolean add(Node[] nodes) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public boolean remove(Node[] nodes) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

    }
}
