/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.steeplesoft.awestruct.netbeans.nodes;

import com.steeplesoft.awestruct.netbeans.AwestructProject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import javax.swing.event.ChangeListener;
import org.netbeans.api.project.Project;
import org.netbeans.spi.project.ui.support.NodeFactory;
import org.netbeans.spi.project.ui.support.NodeList;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.nodes.FilterNode;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;

/**
 *
 * @author jdlee
 */
@NodeFactory.Registration(projectType = "com-steeplesoft-awestruct-netbeans", position = 10)
public class ConfigNodeFactory implements NodeFactory {

    @Override
    public NodeList<?> createNodes(Project project) {
        AwestructProject p = project.getLookup().lookup(AwestructProject.class);
        assert p != null;
        return new ConfigNodeList(p);
    }

    private class ConfigNodeList implements NodeList<Node> {
        private final AwestructProject project;
        public ConfigNodeList(AwestructProject project) {
            this.project = project;
        }

        @Override
        public List<Node> keys() {
            List<Node> result = new ArrayList<Node>();
            Enumeration<? extends FileObject> folders = project.getProjectDirectory().getChildren(false);
            while (folders.hasMoreElements()) {
                try {
                    FileObject folder = folders.nextElement();
                    final String name = folder.getName();
                    if (!name.startsWith(".") && !"_site".equals(name)) {
                        result.add(DataObject.find(folder).getNodeDelegate());
                    }
                } catch (DataObjectNotFoundException ex) {
                    Exceptions.printStackTrace(ex);
                }
            }
            Collections.sort(result, new ConfigNodeComparator());
            return result;
        }

        @Override
        public Node node(Node node) {
            return new FilterNode(node);
        }

        @Override
        public void addNotify() {
        }

        @Override
        public void removeNotify() {
        }

        @Override
        public void addChangeListener(ChangeListener cl) {
        }

        @Override
        public void removeChangeListener(ChangeListener cl) {
        }

    }

}
