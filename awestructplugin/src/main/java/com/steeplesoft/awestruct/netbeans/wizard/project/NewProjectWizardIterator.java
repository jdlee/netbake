/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.steeplesoft.awestruct.netbeans.wizard.project;

import com.steeplesoft.awestruct.netbeans.AwestructProject;
import com.steeplesoft.awestruct.netbeans.processes.ProcessWrapper;
import java.awt.Component;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.LinkedHashSet;
import java.util.NoSuchElementException;
import java.util.Set;
import javax.swing.JComponent;
import javax.swing.event.ChangeListener;
import org.netbeans.api.templates.TemplateRegistration;
import org.netbeans.spi.project.ui.support.ProjectChooser;
import org.openide.WizardDescriptor;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;
import org.openide.util.NbBundle.Messages;
import org.openide.util.NbPreferences;

/**
 *
 * @author jdlee
 */
@TemplateRegistration(folder = "Project/Awestruct",
        displayName = "#displayName",
        description = "NewAwestructProjectDescription.html",
        iconBase = "com/steeplesoft/awestruct/netbeans/icon.png")
@Messages("displayName=Awestruct Site")
public class NewProjectWizardIterator implements WizardDescriptor./*Progress*/InstantiatingIterator {

    private int index;
    private WizardDescriptor.Panel[] panels;
    private WizardDescriptor wiz;

    public static NewProjectWizardIterator createIterator() {
        return new NewProjectWizardIterator();
    }

    @Override
    public Set instantiate() throws IOException {
        Set<FileObject> resultSet = new LinkedHashSet<FileObject>();
        File projDir = new File((String) wiz.getProperty("projDir"));
        File dirF = FileUtil.normalizeFile(projDir);
        dirF.mkdirs();

        // TODO: Move to options to whip up something like the Asciidoctor Java integration
        ProcessWrapper pw = new ProcessWrapper(projDir, //"/usr/local/bin/awestruct",
                NbPreferences.forModule(AwestructProject.class).get(AwestructProject.OPTION_AWESTRUCT_PATH,
                        AwestructProject.DEFAULT_AWESTRUCT_PATH),
                "-i",
                "-f",
                (String) wiz.getProperty("framework")
        );
        try {
            pw.execute();
            BufferedWriter out = null;
            try {
                String siteName = (String) wiz.getProperty("siteName");
                out = new BufferedWriter(new FileWriter(new File(projDir, "_config/site.yml")));

                outputYamlEntry(out, "title", siteName);
                outputYamlEntry(out, "author", (String) wiz.getProperty("authorName"));
                outputYamlEntry(out, "base_url", (String) wiz.getProperty("baseUrl"));
                outputYamlEntry(out, "org", (String) wiz.getProperty("org"));
                outputYamlEntry(out, "name", siteName);
            } finally {
                if (out != null) {
                    try {
                        out.close();
                    } catch (IOException ioe) {

                    }
                }
            }
        } catch (InterruptedException ex) {
            Exceptions.printStackTrace(ex);
        }

        resultSet.add(FileUtil.toFileObject(dirF));

        File parent = dirF.getParentFile();
        if (parent != null && parent.exists()) {
            ProjectChooser.setProjectsFolder(parent);
        }

        return resultSet;
    }

    @Override
    public void initialize(WizardDescriptor wizard) {
        this.wiz = wizard;
        index = 0;
        panels = createPanels();
        // Make sure list of steps is accurate.
        String[] steps = createSteps();
        for (int i = 0; i < panels.length; i++) {
            Component c = panels[i].getComponent();
            if (steps[i] == null) {
                // Default step name to component name of panel.
                // Mainly useful for getting the name of the target
                // chooser to appear in the list of steps.
                steps[i] = c.getName();
            }
            if (c instanceof JComponent) { // assume Swing components
                JComponent jc = (JComponent) c;
                // Step #.
                // TODO if using org.openide.dialogs >= 7.8, can use WizardDescriptor.PROP_*:
                jc.putClientProperty("WizardPanel_contentSelectedIndex", new Integer(i));
                // Step name (actually the whole list for reference).
                jc.putClientProperty("WizardPanel_contentData", steps);
            }
        }
    }

    @Override
    public void uninitialize(WizardDescriptor wizard) {
        this.wiz.putProperty("projdir", null);
        this.wiz.putProperty("name", null);
        this.wiz = null;
        panels = null;
    }

    @Override
    public String name() {
        return MessageFormat.format("{0} of {1}",
                new Object[]{new Integer(index + 1), new Integer(panels.length)});
    }

    @Override
    public boolean hasNext() {
        return index < panels.length - 1;
    }

    @Override
    public boolean hasPrevious() {
        return index > 0;
    }

    @Override
    public void nextPanel() {
        if (!hasNext()) {
            throw new NoSuchElementException();
        }
        index++;
    }

    @Override
    public void previousPanel() {
        if (!hasPrevious()) {
            throw new NoSuchElementException();
        }
        index--;
    }

    @Override
    public WizardDescriptor.Panel current() {
        return panels[index];
    }

    // If nothing unusual changes in the middle of the wizard, simply:
    @Override
    public final void addChangeListener(ChangeListener l) {
    }

    @Override
    public final void removeChangeListener(ChangeListener l) {
    }

    private WizardDescriptor.Panel[] createPanels() {
        return new WizardDescriptor.Panel[]{new NewProjectWizardPanel1()};
    }

    private String[] createSteps() {
        return new String[]{
            NbBundle.getMessage(NewProjectWizardIterator.class, "LBL_CreateProjectStep")
        };
    }

    private void outputYamlEntry(BufferedWriter writer, String key, String value) throws IOException {
        writer.write(String.format("%s: %s", key, value));
        writer.newLine();
    }
}
