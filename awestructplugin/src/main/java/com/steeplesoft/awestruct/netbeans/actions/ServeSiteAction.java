/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.steeplesoft.awestruct.netbeans.actions;

import com.steeplesoft.awestruct.netbeans.AwestructProject;
import com.steeplesoft.awestruct.netbeans.processes.SiteServer;
import java.awt.event.ActionEvent;
import java.io.File;
import javax.swing.AbstractAction;
import javax.swing.Action;
import static javax.swing.Action.NAME;
import org.netbeans.api.project.Project;
import org.openide.awt.DynamicMenuContent;
import org.openide.util.ContextAwareAction;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

/**
 *
 * @author jdlee
 */
@NbBundle.Messages({
    "LBL_Serve_Site=Serve Site",
    "LBL_Stop_Serving_Site=Stop Serving Site"
})
public class ServeSiteAction extends AbstractAction implements ContextAwareAction {
    @Override
    public void actionPerformed(ActionEvent e) {
        assert false;
    }

    @Override
    public Action createContextAwareInstance(Lookup context) {
        return new ContextAction(context);
    }

    private static final class ContextAction extends AbstractAction {
        private final Project project;

        public ContextAction(Lookup context) {
            project = context.lookup(Project.class);
            putValue(DynamicMenuContent.HIDE_WHEN_DISABLED, true);
            setEnabled(project instanceof AwestructProject);
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            final SiteServer instance = SiteServer.instance(project);
            if (instance.isRunning()) {
                instance.stop();
            } else {
                instance.start(project.getProjectDirectory().getPath());
            }
        }

        @Override
        public boolean isEnabled() {
            final SiteServer instance = SiteServer.instance(project);
            if (instance.isRunning()) {
                putValue(NAME, NbBundle.getMessage(ServeSiteAction.class, "LBL_Stop_Serving_Site"));
            } else {
                putValue(NAME, NbBundle.getMessage(ServeSiteAction.class, "LBL_Serve_Site"));
            }

            File siteDir = new File(project.getProjectDirectory().getPath(), "_site");
            return instance.isRunning() || siteDir.exists();
        }

    }
}
