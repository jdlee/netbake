/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.steeplesoft.awestruct.netbeans.nodes;

import java.util.Comparator;
import org.openide.nodes.Node;

/**
 *
 * @author jdlee
 */
public class ConfigNodeComparator implements Comparator<Node> {
    @Override
    public int compare(Node n1, Node n2) {
        if (n1.isLeaf() && !n2.isLeaf()) {
            return 1;
        }
        if (!n1.isLeaf() && n2.isLeaf()) {
            return -1;
        }
        return n1.getName().toLowerCase().compareTo(n2.getName().toLowerCase());
    }
}
