/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.steeplesoft.awestruct.netbeans.processes;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import org.openide.windows.IOProvider;
import org.openide.windows.InputOutput;
import org.openide.windows.OutputWriter;

/**
 *
 * @author jdlee
 */
public class ProcessWrapper {

    private String[] cmdArray;
    private File workingDir;
    private Process proc;

    public ProcessWrapper(String... cmdArray) {
        this((File) null, cmdArray);
    }

    public ProcessWrapper(File workingDir, String... cmdArray) {
        this.cmdArray = cmdArray;
        this.workingDir = workingDir;
    }
    
    public void execute() throws InterruptedException, IOException {
        execute(true);
    }

    public void execute(boolean openNewWindow) throws InterruptedException, IOException {
        proc = Runtime.getRuntime().exec(cmdArray, null, workingDir);
        InputOutput io = IOProvider.getDefault().getIO("Awestruct", true);
        final OutputWriter stderr = io.getErr();
        final OutputWriter stdout = io.getOut();
        io.setOutputVisible(true);

        StreamGrabber errorGrabber = new StreamGrabber(proc.getErrorStream(), stderr);
        StreamGrabber outputGrabber = new StreamGrabber(proc.getInputStream(), stdout);
        errorGrabber.start();
        outputGrabber.start();

        try {
            stdout.println("Executing " + cmdString());
            int exitVal = proc.waitFor();
            if (exitVal != 0 && exitVal != 143) {
                errorGrabber.join();
                throw new RuntimeException();
            } else {
                outputGrabber.join();
            }
        } finally {
            stdout.println("Finished.");
            stderr.close();
            stdout.close();
        }
    }

    public void stop() {
        if (proc != null) {
            proc.destroy();
            proc = null;
        }
    }

    private String cmdString() {
        StringBuilder sb = new StringBuilder();
        String sep = "";
        for (String part : cmdArray) {
            sb.append(sep).append(part);
            sep = " ";
        }

        return sb.toString();
    }

    private static class StreamGrabber extends Thread {

        private final InputStream is;
        private final StringBuilder sb = new StringBuilder();
        private final OutputWriter ow;

        StreamGrabber(InputStream is, OutputWriter ow) {
            this.is = is;
            this.ow = ow;
        }

        @Override
        public void run() {
            try {
                InputStreamReader isr = new InputStreamReader(is);
                BufferedReader br = new BufferedReader(isr);
                String line;
                while ((line = br.readLine()) != null) {
                    ow.println(line);
                }
            } catch (IOException ioe) {
            }
        }
    }
}
