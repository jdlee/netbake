/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.steeplesoft.awestruct.netbeans;

import org.openide.filesystems.FileAttributeEvent;
import org.openide.filesystems.FileChangeListener;
import org.openide.filesystems.FileEvent;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileRenameEvent;

/**
 *
 * @author jdlee
 */
public class NetBakeFileChangeListener implements FileChangeListener {
    private final FileObject projectDir;
    private final AwestructWrapper aw;

    public NetBakeFileChangeListener(FileObject projectDir) {
        this.projectDir = projectDir;
        aw = new AwestructWrapper(projectDir.getPath());
    }

    private void log(String method, FileObject fo) {
        System.out.println("***** " + method + ": " + fo.getName());
    }

    @Override
    public void fileFolderCreated(FileEvent fe) {
        regenerateSite(fe.getFile());
    }

    @Override
    public void fileDataCreated(FileEvent fe) {
        log("fileDataCreated", fe.getFile());
        regenerateSite(fe.getFile());
    }

    @Override
    public void fileChanged(FileEvent fe) {
        log("fileChanged", fe.getFile());
        regenerateSite(fe.getFile());
    }

    @Override
    public void fileDeleted(FileEvent fe) {
        regenerateSite(fe.getFile());
    }

    @Override
    public void fileRenamed(FileRenameEvent fre) {
        regenerateSite(fre.getFile());
    }

    @Override
    public void fileAttributeChanged(FileAttributeEvent fae) {
        regenerateSite(fae.getFile());
    }

    private void regenerateSite(FileObject fo) {
        String fileName = fo.getPath();
        if (!"debug.log".equals(fo.getNameExt()) && !(fo.getPath().contains("_site"))) {
            aw.generateSite(false);
        }
    }
}
