/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.steeplesoft.awestruct.netbeans.properties;

import com.steeplesoft.awestruct.netbeans.AwestructProject;
import com.steeplesoft.awestruct.netbeans.AwestructProjectProperties;
import java.awt.Dialog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.netbeans.api.project.ProjectUtils;
import org.netbeans.spi.project.ui.CustomizerProvider;
import org.netbeans.spi.project.ui.support.ProjectCustomizer;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author jdlee
 */
public class AwestructCustomizerProvider implements CustomizerProvider {
    public final AwestructProject project;
    private AwestructProjectProperties props;

    public static final String CUSTOMIZER_FOLDER_PATH
            = "Projects/com-steeplesoft-awestruct-netbeans/Customizer";

    public AwestructCustomizerProvider(AwestructProject project) {
        this.project = project;
    }

    @Override
    public void showCustomizer() {
        props = new AwestructProjectProperties(project);
        Dialog dialog = ProjectCustomizer.createCustomizerDialog(CUSTOMIZER_FOLDER_PATH,
                Lookups.fixed(project, props), "", new OKOptionListener(), null);
        dialog.setTitle(ProjectUtils.getInformation(project).getDisplayName());
        dialog.setVisible(true);
    }

    private class OKOptionListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            props.save();
        }

    }

}
