/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.steeplesoft.awestruct.netbeans.wizard.project;

import javax.swing.event.ChangeListener;
import org.openide.WizardDescriptor;
import org.openide.WizardValidationException;
import org.openide.util.HelpCtx;

public class NewProjectWizardPanel1 implements WizardDescriptor.ValidatingPanel<WizardDescriptor> {

    /**
     * The visual component that displays this panel. If you need to access the
     * component from this class, just use getComponent().
     */
    private NewProjectVisualPanel1 component;

    // Get the visual component for the panel. In this template, the component
    // is kept separate. This can be more efficient: if the wizard is created
    // but never displayed, or not all panels are displayed, it is better to
    // create only those which really need to be visible.
    @Override
    public NewProjectVisualPanel1 getComponent() {
        if (component == null) {
            component = new NewProjectVisualPanel1();
        }
        return component;
    }

    @Override
    public HelpCtx getHelp() {
        return HelpCtx.DEFAULT_HELP;
    }

    @Override
    public boolean isValid() {
        return true;
    }

    @Override
    public void addChangeListener(ChangeListener l) {
    }

    @Override
    public void removeChangeListener(ChangeListener l) {
    }

    @Override
    public void readSettings(WizardDescriptor wiz) {
        final NewProjectVisualPanel1 comp = getComponent();

        comp.setAuthor((String)wiz.getProperty("authorName"));
        comp.setSiteName((String)wiz.getProperty("siteName"));
        comp.setFramework((String)wiz.getProperty("framework"));
        comp.setProjectDirectory((String)wiz.getProperty("projDir"));
        comp.setOrganization((String)wiz.getProperty("org"));
        comp.setBaseUrl((String)wiz.getProperty("baseUrl"));
    }

    @Override
    public void storeSettings(WizardDescriptor wiz) {
        final NewProjectVisualPanel1 comp = getComponent();
        wiz.putProperty("authorName", comp.getAuthor());
        wiz.putProperty("siteName", comp.getSiteName());
        wiz.putProperty("framework", comp.getFramework());
        wiz.putProperty("projDir", comp.getProjectDirectory());
        wiz.putProperty("org", comp.getOrganization());
        wiz.putProperty("baseUrl", comp.getBaseUrl());
    }

    @Override
    public void validate() throws WizardValidationException {
        if ("".equals(component.getAuthor())) {
            throw new WizardValidationException(null, "Invalid Author Name", null);
        }
        if ("".equals(component.getSiteName())) {
            throw new WizardValidationException(null, "Invalid Site Name", null);
        }
    }

}
