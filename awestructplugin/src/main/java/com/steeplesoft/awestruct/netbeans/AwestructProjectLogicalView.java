/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.steeplesoft.awestruct.netbeans;

import com.steeplesoft.awestruct.netbeans.actions.Actions;
import java.awt.Image;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.util.List;
import javax.swing.Action;
import org.netbeans.api.annotations.common.StaticResource;
import org.netbeans.spi.project.ui.LogicalViewProvider;
import org.netbeans.spi.project.ui.support.CommonProjectActions;
import org.netbeans.spi.project.ui.support.NodeFactorySupport;
import org.openide.actions.FileSystemAction;
import org.openide.actions.FileSystemRefreshAction;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.FilterNode;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.actions.SystemAction;
import org.openide.util.lookup.Lookups;
import org.openide.util.lookup.ProxyLookup;

/**
 *
 * @author jdlee
 */
public class AwestructProjectLogicalView implements LogicalViewProvider {
    @StaticResource
    public static final String AWESTRUCT_ICON = "com/steeplesoft/awestruct/netbeans/icon.png";
    private final AwestructProject project;

    public AwestructProjectLogicalView(AwestructProject project) {
        this.project = project;
    }

    @Override
    public Node createLogicalView() {
        try {
            FileObject projectDirectory = project.getProjectDirectory();
            DataFolder projectFolder = DataFolder.findFolder(projectDirectory);
            Node nodeOfProjectFolder = projectFolder.getNodeDelegate();
            return new AwestructProjectNode(nodeOfProjectFolder, project);
        } catch (DataObjectNotFoundException donfe) {
            Exceptions.printStackTrace(donfe);
            return new AbstractNode(Children.LEAF);
        }
    }

    public final class AwestructProjectNode extends FilterNode {
        private final AwestructProject project;

        public AwestructProjectNode(Node node, AwestructProject project) throws DataObjectNotFoundException {
            super(node,
                    NodeFactorySupport.createCompositeChildren(
                            project, 
                            "Projects/com-steeplesoft-awestruct-netbeans/Nodes"),
//                    new FilterNode.Children(node),
                    new ProxyLookup(
                            new Lookup[]{
                                Lookups.singleton(project),
                                node.getLookup()
                            }));
            this.project = project;
        }

        @Override
        public Action[] getActions(boolean arg0) {
            return new Action[]{
                CommonProjectActions.newFileAction(),
                Actions.generateSiteAction(),
                Actions.serveSiteAction(),
                Actions.deploySiteAction(),
                CommonProjectActions.customizeProjectAction(),
                CommonProjectActions.closeProjectAction(),
                SystemAction.get(FileSystemAction.class),
                SystemAction.get(FileSystemRefreshAction.class)
            };
        }

        @Override
        public Image getIcon(int type) {
            return ImageUtilities.loadImage(AWESTRUCT_ICON);
        }

        @Override
        public Image getOpenedIcon(int type) {
            return getIcon(type);
        }

        @Override
        public String getDisplayName() {
            String displayName = "Awestruct - " + project.getProjectDirectory().getName();
            try {
                List<String> lines = Files.readAllLines(
                        FileSystems.getDefault().getPath(project.getProjectDirectory().getPath(), "_config/site.yml"),
                        Charset.defaultCharset() );
                if (lines != null) {
                    for (String line : lines) {
                        if (line.trim().startsWith("title")) {
                            String title = line.substring(line.indexOf(":")+1);
                            displayName = title.trim() + " (Awestruct)";
                        }
                    }
                }
            } catch (IOException ex) {
            }
            return displayName;
        }

    }

    @Override
    public Node findPath(Node root, Object target) {
        //leave unimplemented for now
        return null;
    }

}
