/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.steeplesoft.awestruct.netbeans.actions;

import javax.swing.Action;

/**
 *
 * @author jdlee
 */
public class Actions {

    private static Action generateSite;
    private static ServeSiteAction serveSite;
    private static DeployAction deploy;

    public static Action generateSiteAction() {
        if (generateSite == null) {
            generateSite =  new GenerateSiteAction();
        }

        return generateSite;
    }

    public static Action serveSiteAction() {
        if (serveSite == null) {
            serveSite = new ServeSiteAction();
        }

        return serveSite;
    }
    
    public static Action deploySiteAction() {
        if (deploy == null) {
            deploy = new DeployAction();
        }

        return deploy;
    }
}
