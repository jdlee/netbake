/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.steeplesoft.awestruct.netbeans;

import com.steeplesoft.awestruct.netbeans.processes.ProcessWrapper;
import java.io.File;
import org.openide.util.Exceptions;
import org.openide.util.NbPreferences;

/**
 *
 * @author jdlee
 */
public class AwestructWrapper {
    private final String projectDir;

    public AwestructWrapper(String projectDir) {
        this.projectDir = projectDir;
    }

    public void startServingSite() {
        /*
        new Thread() {
            @Override
            public void run() {
                pw = new OutputWindowProcessWrapper(new File(projectDir),
                        NbPreferences.forModule(AwestructProject.class).get(AwestructProject.OPTION_AWESTRUCT_PATH,
                                AwestructProject.DEFAULT_AWESTRUCT_PATH),
                        "--auto",
                        "--server",
                        "--profile",
                        "development");
                try {
                    pw.execute();
                } catch (Exception ex) {
                    Exceptions.printStackTrace(ex);
                }
            }
        }.start();
        */
    }

    public void stopServingSite() {
//        pw.stop();
    }

    public void generateSite() {
        generateSite(true);
    }
    
    public void generateSite(final boolean forceShowOutput) {
        new Thread() {
            @Override
            public void run() {
                ProcessWrapper wrapper = new ProcessWrapper(new File(projectDir),
                        NbPreferences.forModule(AwestructProject.class).get(AwestructProject.OPTION_AWESTRUCT_PATH,
                                AwestructProject.DEFAULT_AWESTRUCT_PATH),
                        "-g");
                try {
                    wrapper.execute(forceShowOutput);
                } catch (Exception ex) {
                    Exceptions.printStackTrace(ex);
                }
            }
        }.start();
    }

    public void deploySite() {
        new Thread() {
            @Override
            public void run() {
                ProcessWrapper wrapper = new ProcessWrapper(new File(projectDir),
                        NbPreferences.forModule(AwestructProject.class).get(AwestructProject.OPTION_AWESTRUCT_PATH,
                                AwestructProject.DEFAULT_AWESTRUCT_PATH),
                        "-P", 
                        "production", 
                        "-g", 
                        "--force", 
                        "--deploy");
                try {
                    wrapper.execute();
                } catch (Exception ex) {
                    Exceptions.printStackTrace(ex);
                }
            }
        }.start();
    }
}
