/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.steeplesoft.awestruct.netbeans.processes;

import com.steeplesoft.awestruct.netbeans.AwestructProjectProperties;
import java.io.IOException;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.grizzly.http.server.NetworkListener;
import org.glassfish.grizzly.http.server.StaticHttpHandler;
import org.netbeans.api.project.Project;
import org.openide.util.Exceptions;
import org.openide.windows.IOProvider;
import org.openide.windows.InputOutput;

/**
 *
 * @author jdlee
 */
public class SiteServer {

    private static SiteServer instance;
    private int port;
    private final Project project;
    private boolean running;
    HttpServer httpServer = new HttpServer();
    private InputOutput io;

    private SiteServer(Project project) {
        this.project = project;
    }

    public static SiteServer instance(Project project) {
        if (instance == null) {
            instance = new SiteServer(project);
        }

        return instance;
    }

    public boolean isRunning() {
        return running;
    }

    public void start(String projectPath) {
        try {
            AwestructProjectProperties props = new AwestructProjectProperties(project);
            port = Integer.parseInt(props.getPort());

            NetworkListener networkListener = new NetworkListener("awestruct-listener",
                    "localhost", port);
            httpServer.addListener(networkListener);
            final StaticHttpHandler handler = new StaticHttpHandler(project.getProjectDirectory().getPath() + "/_site", "/");
            handler.setFileCacheEnabled(false);

            httpServer.getServerConfiguration().addHttpHandler(handler);
            io = IOProvider.getDefault().getIO("Awestruct Server", true);
            io.getOut().println("Starting server on port " + port);

            httpServer.start();
            running = true;
        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
        }
    }

    public void stop() {
        io.getOut().println("Shutting down server");
        io.closeInputOutput();

        httpServer.shutdownNow();

        instance = null;
        io = null;
        running = false;
    }
}
