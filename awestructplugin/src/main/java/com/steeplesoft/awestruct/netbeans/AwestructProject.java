package com.steeplesoft.awestruct.netbeans;

import com.steeplesoft.awestruct.netbeans.properties.AwestructCustomizerProvider;
import java.beans.PropertyChangeListener;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import org.netbeans.api.annotations.common.StaticResource;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ProjectInformation;
import org.netbeans.spi.project.ProjectState;
import org.openide.filesystems.FileObject;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author jdlee
 */
public class AwestructProject implements Project {

    public static final String DEFAULT_AWESTRUCT_PATH = "/usr/local/bin/awestruct";
    public static final String DEFAULT_JRUBY_HOME = "/opt/jruby";
    public static final String OPTION_AWESTRUCT_PATH = "awestructPath";
    public static final String OPTION_JRUBY_HOME = "jrubyHome";

    private final FileObject projectDir;
    private final ProjectState state;
    private Lookup lkp;

    public AwestructProject(FileObject projectDir, ProjectState state) {
        this.projectDir = projectDir;
        this.state = state;
        projectDir.addRecursiveListener(new NetBakeFileChangeListener(projectDir));
    }

    @Override
    public FileObject getProjectDirectory() {
        return projectDir;
    }

    @Override
    public Lookup getLookup() {
        if (lkp == null) {
            lkp = Lookups.fixed(new Object[]{
                this,
                new Info(),
                new AwestructProjectLogicalView(this),
                new AwestructCustomizerProvider(this),});
        }
        return lkp;
    }

    private final class Info implements ProjectInformation {

        @StaticResource()
        public static final String AWESTRUCT_ICON = "com/steeplesoft/awestruct/netbeans/icon.png";

        @Override
        public Icon getIcon() {
            return new ImageIcon(ImageUtilities.loadImage(AWESTRUCT_ICON));
        }

        @Override
        public String getName() {
            return getProjectDirectory().getName();
        }

        @Override
        public String getDisplayName() {
            return getName();
        }

        @Override
        public void addPropertyChangeListener(PropertyChangeListener pcl) {
            //do nothing, won't change
        }

        @Override
        public void removePropertyChangeListener(PropertyChangeListener pcl) {
            //do nothing, won't change
        }

        @Override
        public Project getProject() {
            return AwestructProject.this;
        }

    }

}
