/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.steeplesoft.awestruct.netbeans;

import java.util.prefs.Preferences;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ProjectUtils;

/**
 *
 * @author jdlee
 */
public class AwestructProjectProperties {
    public static final String PROPS_PORT = "DEBUG_PORT";
    private final Preferences prefs;
    private String port;

    public AwestructProjectProperties(Project project) {
        prefs = ProjectUtils.getPreferences(project, AwestructProject.class, true);
        port = prefs.get(PROPS_PORT, "4242");
    }
    
    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }
    
    public void save() {
        prefs.put(PROPS_PORT, port);
    }
    
}
