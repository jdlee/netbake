/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.steeplesoft.awestruct.netbeans.properties;

import com.steeplesoft.awestruct.netbeans.AwestructProjectProperties;
import javax.swing.JComponent;
import org.netbeans.api.project.Project;
import org.netbeans.spi.project.ui.support.ProjectCustomizer;
import org.netbeans.spi.project.ui.support.ProjectCustomizer.Category;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

/**
 *
 * @author jdlee
 */
public class GeneralAwestructProperties implements ProjectCustomizer.CompositeCategoryProvider {

    private static final String GENERAL = "General";

    @ProjectCustomizer.CompositeCategoryProvider.Registration(
            projectType = "com-steeplesoft-awestruct-netbeans", position = 10)
    public static GeneralAwestructProperties createGeneral() {
        return new GeneralAwestructProperties();
    }

    @NbBundle.Messages("LBL_Config_General=General")
    @Override
    public Category createCategory(Lookup lkp) {
        return ProjectCustomizer.Category.create(GENERAL, "General",null);
    }

    @Override
    public JComponent createComponent(Category category, Lookup lkp) {
        return new ProjectPropertiesPanel(lkp.lookup(AwestructProjectProperties.class));
    }

}