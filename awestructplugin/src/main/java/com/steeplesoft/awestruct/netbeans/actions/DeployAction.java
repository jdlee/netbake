/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.steeplesoft.awestruct.netbeans.actions;

import com.steeplesoft.awestruct.netbeans.AwestructProject;
import com.steeplesoft.awestruct.netbeans.AwestructWrapper;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Action;
import static javax.swing.Action.NAME;
import org.netbeans.api.project.Project;
import org.openide.awt.DynamicMenuContent;
import org.openide.util.ContextAwareAction;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import static com.steeplesoft.awestruct.netbeans.actions.Bundle.*;

/**
 *
 * @author jdlee
 */
@NbBundle.Messages("LBL_Deploy=Deploy Site")
public class DeployAction extends AbstractAction implements ContextAwareAction {

    @Override
    public void actionPerformed(ActionEvent e) {
        assert false;
    }

    @Override
    public Action createContextAwareInstance(Lookup context) {
        return new ContextAction(context);
    }

    private static final class ContextAction extends AbstractAction {
        private final Project project;

        public ContextAction(Lookup context) {
            project = context.lookup(Project.class);
            putValue(DynamicMenuContent.HIDE_WHEN_DISABLED, true);
            putValue(NAME, LBL_Deploy());
            setEnabled(project instanceof AwestructProject);
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            AwestructWrapper aw = new AwestructWrapper(project.getProjectDirectory().getPath());
            aw.deploySite();
        }

        @Override
        public boolean isEnabled() {
            return (project instanceof AwestructProject);
        }

    }
}
